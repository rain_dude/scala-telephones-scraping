package wicu.telephones

import scala.collection.JavaConversions.asScalaBuffer
import scala.collection.parallel.ParIterable
import scala.collection.parallel.ParMap
import scala.collection.parallel.ParSeq
import scala.collection.parallel.immutable.ParRange
import scala.language.postfixOps

import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.select.Elements

import Types._

/**
 * Downloads data from m.gsmarena.com.
 *
 * @author wicu
 */
object DataDownloader {

  val firstPageNo = 1
  val magicF = "f"
  val magic0P = "0-p"

  def getData: SpecsByFeaturesByModelUrlsByBrandUrls = {
    val document = Jsoup connect ("http://m.gsmarena.com/makers.php3") get
    val brandsElem = document getElementById ("list-brands")
    val jsoupBrandUrlElems = brandsElem getElementsByTag ("a")

    val brandUrlElems = (jsoupBrandUrlElems toSeq) par

    val brandsFirstPagesUrls: Urls = brandUrlElems map (x => x.attr("abs:href"))
    val brandsPagesUrlsByFirstPagesUrls: UrlsByUrls = extractBrandsPagesUrls(brandsFirstPagesUrls)

    if ((brandsFirstPagesUrls size) == (brandsPagesUrlsByFirstPagesUrls size)) {
      println(s"Jest tyle samo marek co grup linków dla nich, czyli ${brandsPagesUrlsByFirstPagesUrls size}.")
    } else {
      sys.error(s"Powinno być tyle samo marek (jest ich ${brandsFirstPagesUrls size}) co grup linków dla nich (jest ich ${brandsPagesUrlsByFirstPagesUrls size}!")
    }

    val modelsUrlsByBransUrls: UrlsByUrls = extractModelsUrlsByBrandsUrlsFrom(brandsPagesUrlsByFirstPagesUrls)

    val specsByFeaturesByModelsUrlsByBrandsUrls: ParMap[String, ParMap[String, ParMap[String, String]]] = getSpecsByFeaturesByModelsUrlsByBrandsUrls(modelsUrlsByBransUrls)

    return specsByFeaturesByModelsUrlsByBrandsUrls
  }

  def extractBrandsPagesUrls(brandFirstPages: ParSeq[String]): ParMap[String, ParSeq[String]] = {
    val allBrandAllUrls: ParMap[String, ParSeq[String]] = brandFirstPages map { x => x -> getBrandUrls(x) } toMap

    allBrandAllUrls foreach { case (k: String, v: ParSeq[String]) => println(s"Ostatnia podstrona ${v last}, bo w sumie jest ${v size} podstron.") }

    return allBrandAllUrls
  }

  def getBrandUrls(brandFirstPage: String): ParSeq[String] = {
    val (brand, phones, number) = deconstructFirstUrl(brandFirstPage)
    val brandPage: Document = Jsoup connect (brandFirstPage) get
    val brandPagesCount = extractPagesCount(brandPage, brand)

    val nextBrandPageIdxs: ParRange = 1 to brandPagesCount par
    val nextBrandPages = nextBrandPageIdxs map { x => s"$brand-$phones-$magicF-$number-$magic0P$x.php" }

    return nextBrandPages
  }

  def deconstructFirstUrl(brandFirstUrl: String): (String, String, String) = {
    val brandNoPHP = brandFirstUrl replaceAll (".php", "")
    val brandUrlParts: Array[String] = brandNoPHP split ("-")

    if ((brandUrlParts size) != 3) {
      sys.error(s"Miałem podzielić $brandFirstUrl na 3 części. Jest ich ${brandUrlParts size}!")
    }

    return (brandUrlParts(0), brandUrlParts(1), brandUrlParts(2))
  }

  def extractPagesCount(page: Document, brand: String): Int = {
    val pagesCountText = extractPagesCountText(page)
    val pagesCount = extractPagesCount(pagesCountText)

    println(s"Dla $brand jest $pagesCount podstron -> $pagesCountText.")

    return pagesCount
  }

  def extractPagesCountText(page: Document): String = {
    val navCurrentElems = page getElementsByClass ("nav-current")
    if ((navCurrentElems size) != 1) {
      sys.error(s"Miał być jeden element typu 'nav-current', a było ${navCurrentElems size}: $navCurrentElems")
    }
    val navCurrentElem = navCurrentElems iterator () next ()

    val spanInNavCurrentElems = navCurrentElem getElementsByTag ("span")
    if ((spanInNavCurrentElems size) != 1) {
      sys.error(s"Miał być jeden element typu 'span', a było ${spanInNavCurrentElems size}: $spanInNavCurrentElems")
    }
    val spanInNavCurrentElem = spanInNavCurrentElems iterator () next ()

    val leftElems = spanInNavCurrentElem getElementsByClass ("left")
    if ((leftElems size) != 1) {
      sys.error(s"Miał być jeden element typu 'left', a było ${leftElems size}: $leftElems")
    }
    val leftElemText = leftElems iterator () next () text ()

    return leftElemText
  }

  def extractPagesCount(pageCountText: String): Int = {
    val leftElemParts = pageCountText split (" ")
    if ((leftElemParts size) != 3) {
      sys.error(s"Miały być 3 części tekstu z elementu 'left', a było ${leftElemParts size}: $leftElemParts")
    }
    val pagesCount = (leftElemParts(2) toInt)

    return pagesCount
  }

  def extractModelsUrlsByBrandsUrlsFrom(brandsPagesUrlsByFirstPagesUrls: ParMap[String, ParSeq[String]]): UrlsByUrls = {
    val modelsUrlsByBrandsFirstPagesUrls = brandsPagesUrlsByFirstPagesUrls map { case (firstPage: String, allPages: ParSeq[String]) => firstPage -> extractModelsUrlsFrom(allPages) } toMap

    (brandsPagesUrlsByFirstPagesUrls keys) foreach { x => println(s"Marka $x ma: ${brandsPagesUrlsByFirstPagesUrls(x) size} podstron i ${modelsUrlsByBrandsFirstPagesUrls(x) size} modeli") }

    return modelsUrlsByBrandsFirstPagesUrls
  }

  def extractModelsUrlsFrom(singleBrandPagesUrls: Urls): Urls = {
    val modelsUrlsFromAllBrandPages = singleBrandPagesUrls flatMap { x => extractModelsUrlsFrom(x) }

    return modelsUrlsFromAllBrandPages
  }

  def extractModelsUrlsFrom(singleBrandSinglePageUrl: String): Urls = {
    val brandPage = Jsoup connect (singleBrandSinglePageUrl) get
    val modelsUrlsElems = (brandPage getElementById ("list-brands") getElementsByTag ("a") toSeq) par
    val modelUrlsFromSinglePage = modelsUrlsElems map { x => x attr ("abs:href") }

    return modelUrlsFromSinglePage
  }

  def getSpecsByFeaturesByModelsUrlsByBrandsUrls(modelsUrlsByBransUrls: UrlsByUrls): ParMap[String, ParMap[String, ParMap[String, String]]] = {
    val modelInfosByModelsUrlsByBrandsUrls = modelsUrlsByBransUrls map { case (k, v) => (k, extractModelInfoFrom(v)) }

    return modelInfosByModelsUrlsByBrandsUrls
  }

  def extractModelInfoFrom(brandModelsUrls: Urls): ByUrls[SpecsByFeatures] = {
    val brandModelsInfos = brandModelsUrls map { x => (x, extractSpecsByFeaturesFrom(x)) } toMap

    return brandModelsInfos
  }

  def extractSpecsByFeaturesFrom(modelUrl: String): SpecsByFeatures = {
    val modelPage = Jsoup connect (modelUrl) get
    val specsElem = modelPage getElementById ("specs-list")
    val tableElems = specsElem getElementsByTag ("table")
    val specsByFeatures: SpecsByFeatures = ((tableElems toSeq) par) flatMap { x => extractFeatureAndSpecFromTable(x, modelUrl) } toMap

    return specsByFeatures
  }

  def extractFeatureAndSpecFromTable(tableElem: Element, modelUrl: String): ParIterable[(String, String)] = {
    val trElems = tableElem getElementsByTag ("tr")
    val trContent: ParMap[Element, Elements] = ((trElems toSeq) par) map { x => (x, x getElementsByTag ("a")) } toMap

    val goodTrContect = trContent filterKeys { x => (trContent(x) size) > 0 }
    val specsByFeaturesElems = goodTrContect map { case (k, v) => (k, v iterator () next ()) }

    val specsByFeatures: ParIterable[(String, String)] = (specsByFeaturesElems keys) map { x => extractFeatureAndSpecFromContent(x, modelUrl) }

    return specsByFeatures
  }

  def extractFeatureAndSpecFromContent(contentElem: Element, modelUrl: String): (String, String) = {
    val tdElems = contentElem getElementsByTag ("td")
    if ((tdElems size) < 2) {
      sys.error(s"Coś nie tego: '$contentElem' -> $modelUrl")
    }

    val tdIterator = tdElems iterator ()
    val feature = tdIterator next () text ()
    val spec = tdIterator next () text ()

    return (feature, spec)
  }
}
