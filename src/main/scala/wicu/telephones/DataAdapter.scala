package wicu.telephones

import java.io.FileOutputStream
import java.io.ObjectOutputStream

import scala.collection.parallel.ParMap
import scala.language.postfixOps

import Types._

/**
 * Provides data. First tests if fresh data is accessible from local file.
 * If true, then gives it. If false then starts with downloading it to new file and then gives it.
 *
 * @author wicu
 */
object DataAdapter {

  def getData: SpecsByFeaturesByModelUrlsByBrandUrls = {
    val maybeData = DataFileExtractor getData

    maybeData match {
      case Some(data) => return data
      case None => return downloadThenSaveThenGiveData
    }
  }

  def downloadThenSaveThenGiveData(): SpecsByFeaturesByModelUrlsByBrandUrls = {
    val data = DataDownloader getData

    val outputStream = new ObjectOutputStream(new FileOutputStream(s"${Files.TodaysFileName}${Files.DatExt}"))
    outputStream.writeObject(data)

    return data
  }
}
