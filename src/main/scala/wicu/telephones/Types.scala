package wicu.telephones

import scala.collection.parallel.ParMap
import scala.collection.parallel.ParSeq

/**
 * @author wicu
 */
object Types {
  type Urls = ParSeq[String]
  type ByUrls[T] = ParMap[String, T]
  type UrlsByUrls = ByUrls[Urls]

  type SpecsByFeatures = ParMap[String, String]

  type SpecsByFeaturesByModelUrlsByBrandUrls = ByUrls[ByUrls[SpecsByFeatures]]
  type ModelInfosByModelUrlsByBrandUrls = ByUrls[ByUrls[ModelInfo]]
}
